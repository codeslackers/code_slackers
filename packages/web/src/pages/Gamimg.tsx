import { Box, Button, Flex, Grid, Heading, Image, Input, Stack, Text, useClipboard } from '@chakra-ui/core';
import React from 'react';

import Fortnite from '../assets/fortnite.jpg';
import Minecraft from '../assets/minecraft.png';
import { LinkButton } from '../components/LinkButton';

const Gaming: React.FC = () => {
  const [value] = React.useState('34.93.14.207');
  const { onCopy, hasCopied } = useClipboard('34.93.14.207');

  return (
    <Stack h={200}>
      <Box mt={200} justifyContent="space-between" px={8}>
        <Box>
          <Heading>Free open source game servers</Heading>
          <Text>
            We believe in gaming freedom! That's why we have make everything
            free and open source.
          </Text>
          <Stack isInline mt={4} spacing={4}>
            <Button variantColor="orange">Request game</Button>
            <Button variant="outline">Book timing</Button>
          </Stack>
        </Box>
      </Box>
      <Grid my={100} gap={4} templateColumns="repeat(3, 1fr)">
        <Box w="100%" shadow="sm" px={6} py={8}>
          <Flex justify="center" flexDirection="column">
            <Box>
              <Image src={Minecraft} alt="Minecraft" h="20" />
            </Box>
            <Text fontSize="2xl">Minecraft Bedrock</Text>
            <Text my={2}>Most popular multiplayer game</Text>
            <Stack>
              <Button size="sm" variantColor="orange">
                See schedule
              </Button>
              <LinkButton
                isExternal={true}
                path="https://gitlab.com/codeslackers/mcpe"
                size="sm"
              >
                Download world
              </LinkButton>
              <Flex align="center">
                <Input
                  size="sm"
                  value={value}
                  isReadOnly
                  placeholder="Welcome"
                />
                <Button size="sm" variantColor="blue" onClick={onCopy} ml={1}>
                  {hasCopied ? 'Copied' : 'Copy'}
                </Button>
              </Flex>
            </Stack>
          </Flex>
        </Box>
        <Box w="100%" shadow="sm" px={6} py={8}>
          <Flex justify="center" flexDirection="column">
            <Box>
              <Image src={Minecraft} alt="Minecraft" h="20" />
            </Box>
            <Text fontSize="2xl">Minecraft Java</Text>
            <Text my={2}>Discontinued</Text>
            <Stack>
              <Button isDisabled={true} size="sm" variantColor="orange">
                See schedule
              </Button>
            </Stack>
          </Flex>
        </Box>
        <Box w="100%" shadow="sm" px={6} py={8}>
          <Flex justify="center" flexDirection="column">
            <Box>
              <Image src={Fortnite} alt="Minecraft" h="20" />
            </Box>
            <Text fontSize="2xl">Fortnite</Text>
            <Text my={2}>Third person battle royale</Text>
            <Stack>
              <LinkButton
                path="https://discord.gg/BNgX5Rz"
                isExternal={true}
                target="_blank"
                size="sm"
                variantColor="orange"
              >
                Join via Discord
              </LinkButton>
            </Stack>
          </Flex>
        </Box>
      </Grid>
      {/* <Box mt={40}>
        <Text fontSize="3xl">Upcomming Events</Text>
        <Divider />
        <Stack>
          <Box>
            <Text>Minecraft Bedrock Survival Hardcore</Text>
            <Text>2rd Jan 2020</Text>
            <Box d="flex" mt="2" alignItems="center">
              {Array(4)
                .fill('')
                .map((_, i) => (
                  <FaStar key={i} />
                ))}
            </Box>
          </Box>
        </Stack>
      </Box> */}
    </Stack>
  );
};

export default Gaming;
