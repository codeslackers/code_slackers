import { Badge, Box, Divider, Flex, Heading, Spinner, Stack, Text } from '@chakra-ui/core';
import axios from 'axios';
import React, { useEffect, useState } from 'react';

export const StatusPage = () => {
  const [loading, setLoading] = useState(true);
  const [error, setErr] = useState(false);
  const [online, setOnline] = useState(false);
  const [playersOnline, setPlayersOnline] = useState(0);

  const onClick = () => {
    axios
      .get('https://gameserver-268313.appspot.com/api/v1/status')
      .then(res => {
        console.log(res.data);
        setLoading(false);
        if (res.data.online!) {
          setOnline(true);
          setPlayersOnline(res.data.player_count);
        }
      })
      .catch(err => {
        console.error(err);
        setErr(true);
      });
  };

  useEffect(() => {
    onClick();
  }, []);

  return (
    <Stack mt={4}>
      <Heading>Server status</Heading>
      <Stack>
        <Flex justify="space-between">
          <Box>Minecraft Bedrock ( {playersOnline} of 10 online )</Box>
          <Box>
            {loading ? (
              <Spinner />
            ) : error ? (
              <Badge variantColor="red">Down</Badge>
            ) : !online ? (
              <Badge variantColor="red">Down</Badge>
            ) : (
              <Badge variantColor="green">Operational</Badge>
            )}
          </Box>
        </Flex>
        <Divider />
      </Stack>
      {/* <Stack pt={20}>
        <Text fontSize="3xl">Past incidents</Text>
        <Text>Coming soon. Contact on discord 0xRizeXor21#2399 for futher issues</Text>
      </Stack> */}
    </Stack>
  );
};
