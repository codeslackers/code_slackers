import { Box, Button, Divider, Flex, Grid, Heading, Stack, Text } from '@chakra-ui/core';
import React from 'react';

import { LinkButton } from '../components/LinkButton';

const Home = () => {
  return (
    <Box as="section">
      <Flex
        py="28vh"
        alignItems={['center', 'center', 'flex-start']}
        flexDirection="column"
      >
        <Heading mb={2}>The place to learn</Heading>
        <Text mb={2}>
          Code Slackers is a community where you can learn to code and have fun
          while learning.
        </Text>
        <Flex>
          <Button mr={2} variantColor="orange">
            Learn more
          </Button>
          <LinkButton path="/blogs">See blogs</LinkButton>
        </Flex>
      </Flex>

      <Divider />

      <Box py={60}>
        <Stack mb={100}>
          <Text fontSize={['2xl', '2xl', '3xl']} as="strong">
            The place to learn all the modern technologies
          </Text>
          <Text>Web development (Django, React), Crash courses (Python)</Text>
          <Grid
            mt={6}
            gap={2}
            gridTemplateColumns={[
              'repeat(1, 1fr)',
              'repeat(1, 3fr)',
              'repeat(3, 1fr)',
            ]}
            alignItems="center"
          >
            <Box>
              <Stack p={4} textAlign="center">
                <Text fontSize="2xl">Blogs</Text>
                <Text mt={2}>
                  Monthly blogs about interesting and new technology.
                </Text>
                <LinkButton
                  mt={8}
                  size="sm"
                  path="/blogs"
                  variantColor="orange"
                >
                  See blogs
                </LinkButton>
              </Stack>
            </Box>
            <Box>
              <Stack textAlign="center">
                <Text fontSize="2xl">Support</Text>
                <Text>
                  24/7 Support via Discord, instantly get help from over 2000
                  members.
                </Text>
                <LinkButton
                  mt={8}
                  size="sm"
                  path="https://discord.gg/BNgX5Rz"
                  variantColor="orange"
                  isExternal={true}
                >
                  Support via Discord
                </LinkButton>
              </Stack>
            </Box>
            <Box>
              <Stack textAlign="center">
                <Text fontSize="2xl">Events</Text>
                <Text>Weekly fun in-game events.</Text>
                <LinkButton
                  mt={8}
                  size="sm"
                  path="/gaming"
                  variantColor="orange"
                >
                  See upcoming events
                </LinkButton>
              </Stack>
            </Box>
          </Grid>
        </Stack>
      </Box>
    </Box>
  );
};

export default Home;
