import { Box, BoxProps } from '@chakra-ui/core';
import React, { FC } from 'react';

const Container: FC<BoxProps> = ({ children, ...props }) => (
  <Box w="90%" mx="auto" maxW={1100} {...props}>
    {children}
  </Box>
);

export default Container;
