import { Box, Button, Image, Link, List, ListItem, useDisclosure } from '@chakra-ui/core';
import React from 'react';
import { TiThMenu } from 'react-icons/ti';

import logo from '../assets/codeslackers.svg';
import { routes } from '../routes';
import Container from './Container';
import Drawer from './Drawer';
import { LinkButton } from './LinkButton';

const Navbar = () => {
  const disclosure = useDisclosure();
  const iconRef = React.createRef<HTMLElement>();

  return (
    <Box as="header" w="100%" p={5} bg={'#1A202C'}>
      <Container d="flex" alignItems="center">
        <Link href="/" transition="max-width .3s" mr="auto">
          <Image src={logo} alt="Logo" maxW="100%" h="auto" w={55} />
        </Link>
        <Box as="nav" d={{ base: 'none', lg: 'block' }}>
          <List d="flex" flexWrap="wrap" fontWeight="bold">
            {routes.map((route, key) => (
              <ListItem d="flex" alignItems="center" key={key}>
                <LinkButton
                  path={route.path}
                  variant="link"
                  d="block"
                  p=".5rem 1rem"
                  lineHeight="unset"
                >
                  {route.name}
                </LinkButton>
              </ListItem>
            ))}
            <ListItem>
              <LinkButton path="/signin" variantColor="orange" ml="1rem">
                Sign in
              </LinkButton>
            </ListItem>
          </List>
        </Box>
        <Button
          onClick={disclosure.onOpen}
          ml="auto"
          d={{ base: 'block', lg: 'none' }}
          ref={iconRef}
          bg="transparent"
          variant="unstyled"
          padding={0}
        >
          <Box as={TiThMenu} size={22} color="white"></Box>
        </Button>
        <Drawer {...disclosure} reference={iconRef} />
      </Container>
    </Box>
  );
};

export default Navbar;
