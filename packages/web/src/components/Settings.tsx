import React from 'react'
import { useColorMode, Button, Box, Text } from '@chakra-ui/core'

export const SettingsComponent = () => {
    const { colorMode, toggleColorMode } = useColorMode();
    return (
        <>
            <Box pt={4}>
                <Text pb={2}>Local Settings:</Text>
                <Button onClick={toggleColorMode}>
                    {colorMode === "light" ? "Dark Mode" : "Light Mode"}
                </Button>
            </Box>
        </>
    )
}
