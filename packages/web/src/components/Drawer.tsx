import {
  Drawer as ChakraDrawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Image,
  Stack,
  Text,
} from '@chakra-ui/core';
import React from 'react';
import { RefObject } from 'react';

import logo from '../assets/codeslackers.svg';
import { routes } from '../routes';
import { LinkButton } from './LinkButton';

interface IDrawerProps {
  onClose: () => void;
  isOpen: boolean;
  reference: RefObject<HTMLElement>;
}

const Drawer = ({ onClose, isOpen, reference }: IDrawerProps) => {
  return (
    <>
      <ChakraDrawer
        isOpen={isOpen}
        placement="right"
        onClose={onClose}
        size="md"
        finalFocusRef={reference}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton onClick={onClose} />
          <DrawerHeader borderBottomWidth="1px">
            <Image src={logo} alt="Logo" w={55} />
          </DrawerHeader>

          <DrawerBody>
            <Stack spacing={2}>
              {routes.map((route, key) => (
                <LinkButton
                  path={route.path}
                  variant="link"
                  d="block"
                  p=".5rem 1rem"
                  lineHeight="unset"
                  key={key}
                >
                  {route.name}
                </LinkButton>
              ))}
              <LinkButton path="/signin" variantColor="orange">
                Sign in
              </LinkButton>
            </Stack>
          </DrawerBody>

          <DrawerFooter justifyContent="center">
            <Text as="strong">With ♥ open source.</Text>
          </DrawerFooter>
        </DrawerContent>
      </ChakraDrawer>
    </>
  );
};

export default Drawer;
