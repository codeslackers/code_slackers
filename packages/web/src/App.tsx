import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Container from './components/Container';
import Navbar from './components/Navbar';
import { SettingsComponent } from './components/Settings';
// import { LoginComponent } from './components/Signin';
import Gaming from './pages/Gamimg';
import Home from './pages/Home';
import { StatusPage } from './pages/Status';

function App() {
  return (
    <Router>
      <Navbar />
      <Container>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/gaming" component={Gaming} />
          <Route exact path="/status" component={StatusPage} />
          <Route exact path="/settings" component={SettingsComponent} />
          {/*<Route exact path="/signin" component={LoginComponent} /> */}
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
