import { ApolloProvider } from '@apollo/react-hooks';
import { ColorModeProvider, CSSReset, ThemeProvider } from '@chakra-ui/core';
import ApolloClient, { InMemoryCache } from 'apollo-boost';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import { theme } from './theme';

const client = new ApolloClient({
  uri: 'https://48p1r2roz4.sse.codesandbox.io',
  credentials: 'include',
  cache: new InMemoryCache(),
});

const ThemedApp = () => (
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <ColorModeProvider>
        <CSSReset />
        <App />
      </ColorModeProvider>
    </ThemeProvider>
  </ApolloProvider>
);

ReactDOM.render(<ThemedApp />, document.getElementById('root'));
