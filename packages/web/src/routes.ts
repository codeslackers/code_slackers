export const routes = [
  {
    path: '/blogs',
    name: 'Blogs',
  },
  {
    path: '/gaming',
    name: 'Gaming',
  },
  {
    path: '/status',
    name: 'Status',
  },
  {
    path: '/settings',
    name: 'Settings',
  },
];
