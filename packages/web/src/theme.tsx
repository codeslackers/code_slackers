import { DefaultTheme, theme as defaultTheme } from '@chakra-ui/core';
import React from 'react';

const mappings = {
  sm: '480px',
  md: '768px',
  lg: '960px',
  xl: '1140px',
};

const breakpoints = Object.assign(Object.values(mappings), mappings);

export const theme: DefaultTheme = {
  fontWeights: {
    normal: 400,
    medium: 500,
    bold: 700,
  },
  colors: {
    orange: '#EE964B',
    gray: {
      50: '#faf9f9',
      100: '#efedeb',
      200: '#e4e0dd',
      300: '#d8d2cd',
      400: '#cac2bc',
      500: '#bcb1aa',
      600: '#ab9d94',
      700: '#96867a',
      800: '#79695e',
      900: '#473e37',
    },
  },
  breakpoints,
  icons: {
    ...defaultTheme.icons,
  },
  ...defaultTheme,
};
