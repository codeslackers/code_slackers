import { Ctx, Query, Resolver } from 'type-graphql'

import { IContext } from '../types'

@Resolver()
export class HelloResolver {
  @Query(() => String)
  hello(@Ctx() { client }: IContext) {
    return `${client.user!.username} says hello!`;
  }
}
