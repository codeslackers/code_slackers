import { Field, ObjectType } from 'type-graphql'

@ObjectType()
export class User {
  @Field()
  username: string

  @Field()
  id: string
}
