import './strategy'

import { Router } from 'express'
import passport from 'passport'

export const router = Router();

router.get('/auth/discord', passport.authenticate('discord'));

router.get(
  '/auth/discord/callback',
  passport.authenticate('discord', {
    failureRedirect: process.env.SERVER_DOMAIN || 'http://localhost:3000',
  }),
  (_, res) => {
    return res.redirect(
      process.env.REDIRECT_URL || 'http://localhost:3000/dashboard'
    );
  }
);
