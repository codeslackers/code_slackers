import { Client } from 'discord.js'
import { Request, Response } from 'express'

export interface IContext {
  req: Request;
  res: Response;
  client: InstanceType<typeof Client>;
}
