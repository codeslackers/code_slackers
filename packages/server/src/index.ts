import 'dotenv/config';
import 'reflect-metadata';

import { ApolloServer } from 'apollo-server-express';
import cors from 'cors';
import Express from 'express';
import mongoose from 'mongoose';
import passport from 'passport';
import { buildSchema } from 'type-graphql';

import { router as discordRouter } from './services/discord/api';

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const main = async () => {
  const schema = await buildSchema({
    resolvers: [__dirname + '/resolvers/*{.js,.ts}'],
  });

  const apolloServer = new ApolloServer({
    schema,
    context: ({ req, res }) => ({ req, res }),
  });

  const app = Express();

  app.use(passport.initialize());
  app.use(passport.session());
  app.use(
    cors({
      origin: process.env.SERVER_DOMAIN || 'http://localhost:3000',
      credentials: true,
    })
  );

  const connection = mongoose.createConnection(process.env.MONGO_URI!, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  });

  app.use(
    session({
      name: 'aid',
      secret: process.env.SESSION_SECRET!,
      resave: false,
      saveUninitialized: false,
      cookie: {
        secure: process.env.NODE_ENV === 'production',
        httpOnly: true,
      },
      store: new MongoStore({ mongooseConnection: connection }),
    })
  );

  apolloServer.applyMiddleware({ app, cors: false });

  app.use(discordRouter);

  app.listen(4000, () => {
    console.log(`Server started on port 4000`);
  });
};

main();
